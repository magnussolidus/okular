# translation of org.kde.active.documentviewer.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2012, 2016.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: org.kde.active.documentviewer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:44+0000\n"
"PO-Revision-Date: 2021-07-17 12:09+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.07.80\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "Záložky"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr ""

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr ""

#: package/contents/ui/main.qml:23 package/contents/ui/main.qml:64
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:40
msgid "Open..."
msgstr "Otvoriť..."

#: package/contents/ui/main.qml:47
msgid "About"
msgstr "O"

#: package/contents/ui/main.qml:104
msgid "Password Needed"
msgstr ""

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "Odstrániť záložku"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "Pridať túto stránku do záložiek"

#: package/contents/ui/MainView.qml:82
msgid "No document open"
msgstr "Žiadny dokument nie je otvorený"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "Miniatúry"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "Obsah"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:100
#, fuzzy
#| msgid "No document open"
msgid "Document Version"
msgstr "Žiadny dokument nie je otvorený"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr ""

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr ""

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr ""

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "Nenájdené žiadne výsledky."

#~ msgid "Search..."
#~ msgstr "Hľadať..."

#~ msgid ""
#~ "No Documents found. To start to read, put some files in the Documents "
#~ "folder of your device."
#~ msgstr ""
#~ "Nenašli sa žiadne dokumenty. Na začatie čítania vložte nejaké súbory do "
#~ "priečinka dokumentov vášho zariadenia."

#~ msgid "Document viewer for Plasma Active using Okular"
#~ msgstr "Prehliadač dokumentov pre Plasma Active cez Okular"

#~ msgid "Reader"
#~ msgstr "Čítačka"

#~ msgid "Copyright 2012 Marco Martin"
#~ msgstr "Copyright 2012 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "URL of the file to open"
#~ msgstr "URL súboru na otvorenie"
